export default {
  alias: {
    $components: "./src/components",
    $layouts: "./src/layouts",
    $: "./src",
    $articles: ".src/articles"
  }
}