**IMPORTANT: Please do not create a pull request without creating an issue first.**

Closes #XXXX.

## Description
<!-- Summarize the article's content -->

## Why is this important?
<!-- Tell why this feature should be implemented -->

## The feature implementation is...

- [ ] A simple implementation
- [ ] An implementations that is breaking change 

## Checklist

- [ ] I have read the **CONTRIBUTING** document.
- [ ] An issue for this feature is submitted before making a merge request.
- [ ] I have updated the changelog.
- [ ] I have tested the code.
- [ ] I have linted the code.