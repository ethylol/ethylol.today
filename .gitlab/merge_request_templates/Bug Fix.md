**IMPORTANT: Please do not create a pull request without creating an issue first.**

Closes #XXXX.

## Description
<!-- Summarize the bug fix -->

## Checklist

- [ ] I have read the **CONTRIBUTING** document.
- [ ] An issue for this bug is submitted before making a merge request.
- [ ] I have updated the changelog.
- [ ] I have tested the code.
- [ ] I have linted the code.