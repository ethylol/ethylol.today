**IMPORTANT: Please do not create a pull request without creating an issue first.**

Closes #XXXX.

## Description
<!-- Summarize the article's content -->

## Checklist

- [ ] I have read the **CONTRIBUTING** document.
- [ ] I have submitted an issue for this article before making a merge request.
- [ ] I have included the appropriate metadata in the front matter section.
- [ ] I have checked that the article is up to standards as stated in the **STANDARDS** document.