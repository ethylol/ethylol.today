## Article Idea

<!--
  State your idea here.
-->

(Summarize your working idea for an article)

## Why is it relevant?

<!--
  Explain how this idea is relevant to current affairs.
  You don't have to explain the joke, just provide context
  why your idea would work.
-->

(Explain the relevance of your idea here)

## Does it require prior knowledge?

* [ ] High School Level
* [ ] University Level
* [ ] PhD Level

* [ ] Can be verified with reputable internet articles
* [ ] Can only be verified with scholarly articles