# A guide for contributing to Ethylol Today!

First of all, thank you for considering contributing to Ethylol! 

Following this guidelines is essential to have a frictionless experience when contributing to the project. All members of our community are expected to read and follow our [Code of Conduct](CODE_OF_CONDUCT.md).

## What are avenues where I can contribute?

In Ethylol, there are many ways you can contribute! Here are the following things that you can do to help:

* Report a bug
* Suggest a feature
* Submit an article
* Improve Documentation
* Review submissions
* Edit and fix typos, etc.
* And so many more

## Your First Contribution

There are many ways you can contribute to this project as a beginner of this project. You can:

1. Report Bugs
2. Suggest Features
3. Fix Typos

If you have never contributed to open source before, you can read the following resources:

* [How to Contribute to an Open Source Project on GitHub](https://kcd.im/pull-request) (This is a tutorial for GitHub, but you can apply the concepts on Gitlab)
* [First Timers Only!](https://www.firsttimersonly.com/)

## Getting Started

First, you can browse the issue tracker check for issues with the label "For beginners" and "Help wanted". The "For beginners" labels are issues that needs minimal work, while "Help wanted" labels are issues that need more involvement.

## Reporting bugs

TODO: make up a methodology

## Suggesting features

TODO: make up a methodology

## Review Process

TODO: make up a methodology

## Community

You can talk to the community at the [Ethylol.Today Zulip Chat][Zulip Chat]

## Contributing as a writer

Before contributing to Ethylol as a writer, you must know the following tools and formats:

- Git
- Markdown

That's it! If you do not know Git, here are the following resources to help learn more:

- [git - the simple guide](https://rogerdudler.github.io/git-guide/)
- [The Git site](https://git-scm.com/)

If you do not know Markdown, here is a [website](https://www.markdownguide.org/) for you to read.

## Submitting an Article

To submit an article, you must follow the following process:

1. Open an issue on the repository with "ARTICLE IDEA:" appended to your working title.
2. A maintainer will check your idea to see if it works.
3. If your idea is eligible for submission, please make a merge request.
  1. Write your submission in a Markdown file.
  2. Place it on `./src/articles`.
  3. If you are a first time contributor, please write an author metadata file in `json` on `./src/authors`.
  4. Write the appropriate metadata in the Markdown file.
4. A maintainer will check if your article is up to standards.
5. After your article is reviewed, your merge request may either be accepted or rejected.
  1. If your article is rejected, you may be guided by maintainers in editing your content.
  2. If your article is accepted, your article will be displayed in our website!
6. See your article in the website!

If you find the above instructions too confusing, you can reach us at [Zulip][Zulip Chat].

## Contributing as a developer

You can contribute in many different ways as a developer. But first: you must have the  following installed:

* Git
* Node
* npm

To install and run the site in development mode, do the following commands:

```bash
git clone git@gitlab.com:ethylol/ethylol.today.git
cd "ethylol.today"
npm install
npm run dev
```

To build the site, do `npm run build`.

### Resolving Bugs

TODO: make up a methodology

### Improve documentation

TODO: make up a methodology

## On Git Workflow

Essentially, the git workflow of Ethylol needs to meet the following requirements:

* Releases. Ethylol is not in continuous deployment, and will release new content in intervals that can change if needed.
* Ease of contribution. Traditional workflows such as Git flow is too complicated, and some parts are unnecessary for our use case (e.g. development and hotfix branch).

As such, the Minimal Release-based Workflow is developed. The details are described as follows:

1. There are two long-lived branches: the production branch and the trunk. This is analogue to the main branch and the development branch. One advantage of this is that the development branch is the default. Deploying to production is a conscious decision.
2. Ignoring the production branch, the git workflow works exactly like Github Flow. Fork from the trunk, create some commits, make a pull request, and merge into the trunk. No release branch. No hotfix branch. No develop branch. It's that simple! Branching trunk into production is the maintainer's decision, and as such, you do not have to think about it.

You may have some questions. First, why have a separate production branch? The reason is that I do not want to fiddle with build hooks in Netlify. I would use git tags, but I have to write a CI file and documentations are too confusing! Additionally, I would want anyone to be able to build this website regardless of any Git service you use.

## On Semantic Versioning

Ethylol uses a semantic version system internally. This means that any changes in external interfaces, such as the appearance and organization of the website proper does not affect versioning in any way. However, any change in the metadata and front matter of Markdown articles and JSON author files will also change the version for Ethylol.

### PATCH Change

A change is considered to be a patch change when:

* A new article is submitted.
* A new author file is created.
* A restructuring of front-end stuff, major or minor, is done without altering the structure of content files.
* An article or author file is made.

### MINOR Change

A change is considered to be a minor change when:

* It does not require any updates on other existing files.
* An article is removed.
* A feature is added without breaking existing content files.

### MAJOR Change

A change is considered to be a minor change when:

* A major restructuring of content files is required.
* An author file and all of its articles are removed.

### When to increment version

The version will only increment when it is time to deploy to production.

[Zulip Chat]: https://ethyloltoday.zulipchat.com