# Ethylol Today

[![project chat](https://img.shields.io/badge/zulip-join_chat-brightgreen.svg)](https://ethyloltoday.zulipchat.com)

Official site for Ethylol Today.

Ethylol Today is a satirical news site that focuses on science and technology.

# Installation

You must have Node and npm installed.

To run this site, run the following command:

```bash
npm install
npm run dev
```

To build the site, simply run `npm run build` and check `./dist`.

To deploy this site to the interwebs, see documentation for your favorite static site deployment solution.

# Contributing

See [the contributing guide](CONTRIBUTING.md) for more.

# License

The project's source code is licensed under the [GNU Affero General Public License](https://choosealicense.com/licenses/agpl-3.0/). The project's articles and other content are licensed under [CC BY-SA 4.0](http://creativecommons.org/licenses/by-sa/4.0/). The fonts used are Grenze and Lora, which are both licensed under [SIL Open Font License 1.1](http://scripts.sil.org/OFL).