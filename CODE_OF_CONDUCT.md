* Be friendly
* Be welcoming
* Be patient in explaining things
* Approach disagreements in a healthy manner
* Avoid bad-faith arguments
* Learn to have fun!